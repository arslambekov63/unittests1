package UnitTests.tests;

import UnitTests.PastQuiz.quiz.spaceships.CommandCenter;
import UnitTests.PastQuiz.quiz.spaceships.Spaceship;
import UnitTests.PastQuiz.quiz.spaceships.SpaceshipFleetManager;

import java.util.ArrayList;

public class SpaceshipFleetManagerTest {
    ArrayList<Spaceship> spaceships = new ArrayList<>();
    SpaceshipFleetManager center = new CommandCenter();
    public static void main(String[] args) {
        SpaceshipFleetManagerTest test= new SpaceshipFleetManagerTest();
        System.out.println("Your points is: " + countPoints(test)); //1 тест - 1 балл
    }
    private static int testGetMostPowerfulShip(SpaceshipFleetManagerTest test) {
        int points = 0;
        boolean[] testResults = new boolean[3];
        System.out.println("getMostPowerfulShip tests:");
        System.out.print("test1: ");
        testResults[0] = test.getMostPowerfulShip_isReturnsMostPowerful();
        System.out.println(testResults[0]);
        System.out.print("test2: ");
        testResults[1] = test.getMostPowerfulShip_isReturnsFirst();
        System.out.println(testResults[1]);
        System.out.print("test3: ");
        testResults[2] = test.getMostPowerfulShip_isReturnsNullWhenAllCiviliansShips();
        System.out.println(testResults[2]);
        for (boolean isTrue : testResults) {
            if (isTrue) {
                points++;
            }
        }
        return points;
    }
    private static int testGetShipByName(SpaceshipFleetManagerTest test) {
        int points = 0;
        boolean[] testResult = new boolean[2];
        System.out.println("getShipByName tests:");
        System.out.print("test1: ");
        testResult[0] = test.getShipByName_isReturnsShipByName();
        System.out.println(testResult[0]);
        System.out.print("test2: ");
        testResult[1] = test.getShipByName_isReturnsNullIfNotFound();
        System.out.println(testResult[1]);
        for (boolean isTrue : testResult) {
            if (isTrue) {
                points++;
            }
        }
        return points;
    }
    private static int testGetAllCiviliansShip(SpaceshipFleetManagerTest test) {
        int points = 0;
        boolean[] testResult = new boolean[2];
        System.out.println("GetAllCiviliansShip tests:");
        System.out.print("test1: ");
        testResult[0] = test.getAllCiviliansShips_isReturnsAllCiviliansShips();
        System.out.println(testResult[0]);
        System.out.print("test2: ");
        testResult[1] = test.getAllCiviliansShips_isReturnNullWhenNoCiviliansShips();
        System.out.println(testResult[1]);
        for (boolean isTrue : testResult) {
            if (isTrue) {
                points++;
            }
        }
        return points;
    }
    private static int testGetAllShipsWithEnoughCargoSpace(SpaceshipFleetManagerTest test) {
        int points = 0;
        boolean[] testResult = new boolean[2];
        System.out.println("getAllShipsWithEnoughCargoSpace tests:");
        System.out.print("test1: ");
        testResult[0] = test.getAllShipsWithEnoughCargoSpace_isReturnsAllShipsWithEnoughCargoSpace();
        System.out.println(testResult[0]);
        System.out.print("test2: ");
        testResult[1] = test.getAllShipsWithEnoughCargoSpace_isReturnsNullWhenNoShipsWithEnoughCargoSpace();
        System.out.println(testResult[1]);
        for (boolean isTrue : testResult) {
            if (isTrue) {
                points++;
            }
        }
        return points;
    }
    private static int countPoints(SpaceshipFleetManagerTest test) {
        int points;
        points =  testGetMostPowerfulShip(test);
        points = points + testGetShipByName(test);
        points = points + testGetAllShipsWithEnoughCargoSpace(test);
        points = points + testGetAllCiviliansShip(test);
        return points;
    }

    private boolean getMostPowerfulShip_isReturnsNullWhenAllCiviliansShips() {
        spaceships.add(new Spaceship("foo", 0, 0,0));
        spaceships.add(new Spaceship("bar", 0,0,0));
        spaceships.add(new Spaceship("baz",0,0,0));
        Spaceship result = center.getMostPowerfulShip(spaceships);
        if (result==null) {
            spaceships.clear();
            return true;
        } else {
            spaceships.clear();
            return false;
        }
    }
        //Возвращает самый хорошо вооруженный корабль
    private boolean getMostPowerfulShip_isReturnsMostPowerful() {
        spaceships.add(new Spaceship("foo", 45, 0,0));
        spaceships.add(new Spaceship("bar", 10200,0,0));
        spaceships.add(new Spaceship("baz",456,0,0));
        Spaceship result = center.getMostPowerfulShip(spaceships);
        if (result.getName().equals("bar")&result.getFirePower()==10200) {
            spaceships.clear();
            return true;
        } else {
            spaceships.clear();
            return false;
        }
    }
    //случай когда несколько кораблей с одинаковой мошью, должен возврашаться первый
    private boolean getMostPowerfulShip_isReturnsFirst() {
        spaceships.add(new Spaceship("foo", 1000, 0,0));
        spaceships.add(new Spaceship("bar", 1000,0,0));
        Spaceship result = center.getMostPowerfulShip(spaceships);
        if (result.getName().equals("foo")&result.getFirePower()==1000) {
            spaceships.clear();
            return true;
        } else {
            spaceships.clear();
            return false;
        }
    }
    private boolean getShipByName_isReturnsShipByName() {
        spaceships.add(new Spaceship("foo", 0, 0,0));
        spaceships.add(new Spaceship("bar", 0,0,0));
        Spaceship result = center.getShipByName(spaceships, "bar");
        if (result!=null && result.getName().equals("bar")) {
            spaceships.clear();
            return true;
        } else {
            spaceships.clear();
            return false;
        }
    }
    private boolean getShipByName_isReturnsNullIfNotFound(){
        spaceships.add(new Spaceship("foo", 0, 0,0));
        spaceships.add(new Spaceship("bar", 0,0,0));
        Spaceship result = center.getShipByName(spaceships, "qzw");
        if (result==null) {
            spaceships.clear();
            return true;
        } else {
            spaceships.clear();
            return false;
        }
    }
    private boolean getAllShipsWithEnoughCargoSpace_isReturnsAllShipsWithEnoughCargoSpace() {
        Spaceship bar = new Spaceship("bar", 0,500,0);
        Spaceship baz = new Spaceship("baz", 0,1000,0);
        spaceships.add(new Spaceship("foo", 0, 45,0));
        spaceships.add(bar);
        spaceships.add(baz);
        ArrayList<Spaceship> shipsWithEnoughCargoSpace = new ArrayList<>();
        shipsWithEnoughCargoSpace.add(bar);
        shipsWithEnoughCargoSpace.add(baz);
        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(spaceships,500);
        if (result.equals(shipsWithEnoughCargoSpace)) {
            spaceships.clear();
            return true;
        } else {
            spaceships.clear();
            return false;
        }
    }
    private boolean getAllShipsWithEnoughCargoSpace_isReturnsNullWhenNoShipsWithEnoughCargoSpace() {
        spaceships.add(new Spaceship("foo", 0, 0,0));
        spaceships.add(new Spaceship("bar", 0,0,0));
        ArrayList<Spaceship> result = center.getAllShipsWithEnoughCargoSpace(spaceships,500);
        if (result==null) {
            spaceships.clear();
            return true;
        } else {
            spaceships.clear();
            return false;
        }
    }
    private boolean getAllCiviliansShips_isReturnsAllCiviliansShips() {

        Spaceship foo = new Spaceship("foo", 0, 0,0);
        Spaceship bar = new Spaceship("bar", 0, 0,0);
        ArrayList<Spaceship> check = new ArrayList<>();
        check.add(foo);
        check.add(bar);
        spaceships.add(foo);
        spaceships.add(bar);
        spaceships.add(new Spaceship("baz",456,0,0));
        ArrayList<Spaceship> result = center.getAllCivilianShips(spaceships);
        if (check.equals(result)) {
            spaceships.clear();
            return true;
        } else {
            spaceships.clear();
            return false;
        }
    }
    private boolean getAllCiviliansShips_isReturnNullWhenNoCiviliansShips(){
        spaceships.add(new Spaceship("foo",132,0,0));
        spaceships.add(new Spaceship("bar",799,0,0));
        ArrayList<Spaceship> result = center.getAllCivilianShips(spaceships);
        if (result==null) {
            spaceships.clear();
            return true;
        } else {
            spaceships.clear();
            return false;
        }
    }
}
